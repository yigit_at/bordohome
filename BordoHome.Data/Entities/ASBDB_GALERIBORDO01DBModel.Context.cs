﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BordoHome.Data.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class ASBDB_GALERIBORDO01Entities : DbContext
    {
        public ASBDB_GALERIBORDO01Entities()
            : base("name=ASBDB_GALERIBORDO01Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C_uA_ToplamMusteri> C_uA_ToplamMusteri { get; set; }
        public virtual DbSet<Perkon_FiyatGor> Perkon_FiyatGor { get; set; }
        public virtual DbSet<Perkon_Urunler> Perkon_Urunler { get; set; }
        public virtual DbSet<vwTimur> vwTimurs { get; set; }
        public virtual DbSet<vwTimurME> vwTimurMEs { get; set; }
    
        [DbFunction("ASBDB_GALERIBORDO01Entities", "GetTahsilatStokGroup")]
        public virtual IQueryable<GetTahsilatStokGroup_Result> GetTahsilatStokGroup(string sirketNo, Nullable<System.DateTime> ilkTarih, Nullable<System.DateTime> sonTarih)
        {
            var sirketNoParameter = sirketNo != null ?
                new ObjectParameter("sirketNo", sirketNo) :
                new ObjectParameter("sirketNo", typeof(string));
    
            var ilkTarihParameter = ilkTarih.HasValue ?
                new ObjectParameter("ilkTarih", ilkTarih) :
                new ObjectParameter("ilkTarih", typeof(System.DateTime));
    
            var sonTarihParameter = sonTarih.HasValue ?
                new ObjectParameter("sonTarih", sonTarih) :
                new ObjectParameter("sonTarih", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<GetTahsilatStokGroup_Result>("[ASBDB_GALERIBORDO01Entities].[GetTahsilatStokGroup](@sirketNo, @ilkTarih, @sonTarih)", sirketNoParameter, ilkTarihParameter, sonTarihParameter);
        }
    
        [DbFunction("ASBDB_GALERIBORDO01Entities", "MetinParcala")]
        public virtual IQueryable<MetinParcala_Result> MetinParcala(string metin, string ayrac)
        {
            var metinParameter = metin != null ?
                new ObjectParameter("Metin", metin) :
                new ObjectParameter("Metin", typeof(string));
    
            var ayracParameter = ayrac != null ?
                new ObjectParameter("Ayrac", ayrac) :
                new ObjectParameter("Ayrac", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<MetinParcala_Result>("[ASBDB_GALERIBORDO01Entities].[MetinParcala](@Metin, @Ayrac)", metinParameter, ayracParameter);
        }
    
        public virtual int AsbDropColumn(string tableName, string columnName)
        {
            var tableNameParameter = tableName != null ?
                new ObjectParameter("tableName", tableName) :
                new ObjectParameter("tableName", typeof(string));
    
            var columnNameParameter = columnName != null ?
                new ObjectParameter("columnName", columnName) :
                new ObjectParameter("columnName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AsbDropColumn", tableNameParameter, columnNameParameter);
        }
    
        public virtual ObjectResult<GetWebCiro_Result> GetWebCiro(string beginDate, string endDate)
        {
            var beginDateParameter = beginDate != null ?
                new ObjectParameter("beginDate", beginDate) :
                new ObjectParameter("beginDate", typeof(string));
    
            var endDateParameter = endDate != null ?
                new ObjectParameter("endDate", endDate) :
                new ObjectParameter("endDate", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetWebCiro_Result>("GetWebCiro", beginDateParameter, endDateParameter);
        }
    
        public virtual int PUANTAJKOPYALASTP(string pSirketNo, Nullable<int> pYil, Nullable<int> pAy)
        {
            var pSirketNoParameter = pSirketNo != null ?
                new ObjectParameter("pSirketNo", pSirketNo) :
                new ObjectParameter("pSirketNo", typeof(string));
    
            var pYilParameter = pYil.HasValue ?
                new ObjectParameter("pYil", pYil) :
                new ObjectParameter("pYil", typeof(int));
    
            var pAyParameter = pAy.HasValue ?
                new ObjectParameter("pAy", pAy) :
                new ObjectParameter("pAy", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("PUANTAJKOPYALASTP", pSirketNoParameter, pYilParameter, pAyParameter);
        }
    
        public virtual int sp_depoenvanter()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_depoenvanter");
        }
    
        public virtual int sp_depoenvanter_marka()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_depoenvanter_marka");
        }
    
        public virtual int sp_GeriGonder(string birim, Nullable<int> durum, string alisNo, string durumText)
        {
            var birimParameter = birim != null ?
                new ObjectParameter("Birim", birim) :
                new ObjectParameter("Birim", typeof(string));
    
            var durumParameter = durum.HasValue ?
                new ObjectParameter("Durum", durum) :
                new ObjectParameter("Durum", typeof(int));
    
            var alisNoParameter = alisNo != null ?
                new ObjectParameter("AlisNo", alisNo) :
                new ObjectParameter("AlisNo", typeof(string));
    
            var durumTextParameter = durumText != null ?
                new ObjectParameter("DurumText", durumText) :
                new ObjectParameter("DurumText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_GeriGonder", birimParameter, durumParameter, alisNoParameter, durumTextParameter);
        }
    
        public virtual int sp_KarlilikAnaGrup(Nullable<System.DateTime> basTarih, Nullable<System.DateTime> sonTarih)
        {
            var basTarihParameter = basTarih.HasValue ?
                new ObjectParameter("BasTarih", basTarih) :
                new ObjectParameter("BasTarih", typeof(System.DateTime));
    
            var sonTarihParameter = sonTarih.HasValue ?
                new ObjectParameter("SonTarih", sonTarih) :
                new ObjectParameter("SonTarih", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_KarlilikAnaGrup", basTarihParameter, sonTarihParameter);
        }
    
        public virtual int sp_KarlilikMarka(Nullable<System.DateTime> basTarih, Nullable<System.DateTime> sonTarih)
        {
            var basTarihParameter = basTarih.HasValue ?
                new ObjectParameter("BasTarih", basTarih) :
                new ObjectParameter("BasTarih", typeof(System.DateTime));
    
            var sonTarihParameter = sonTarih.HasValue ?
                new ObjectParameter("SonTarih", sonTarih) :
                new ObjectParameter("SonTarih", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_KarlilikMarka", basTarihParameter, sonTarihParameter);
        }
    
        public virtual int sp_KarlilikMaster(string branch, Nullable<System.DateTime> basTarih, Nullable<System.DateTime> sonTarih)
        {
            var branchParameter = branch != null ?
                new ObjectParameter("branch", branch) :
                new ObjectParameter("branch", typeof(string));
    
            var basTarihParameter = basTarih.HasValue ?
                new ObjectParameter("BasTarih", basTarih) :
                new ObjectParameter("BasTarih", typeof(System.DateTime));
    
            var sonTarihParameter = sonTarih.HasValue ?
                new ObjectParameter("SonTarih", sonTarih) :
                new ObjectParameter("SonTarih", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_KarlilikMaster", branchParameter, basTarihParameter, sonTarihParameter);
        }
    
        public virtual ObjectResult<sp_KasaRaporu_Result> sp_KasaRaporu(Nullable<System.DateTime> basTarih, Nullable<System.DateTime> sonTarih, string magazalar)
        {
            var basTarihParameter = basTarih.HasValue ?
                new ObjectParameter("BasTarih", basTarih) :
                new ObjectParameter("BasTarih", typeof(System.DateTime));
    
            var sonTarihParameter = sonTarih.HasValue ?
                new ObjectParameter("SonTarih", sonTarih) :
                new ObjectParameter("SonTarih", typeof(System.DateTime));
    
            var magazalarParameter = magazalar != null ?
                new ObjectParameter("Magazalar", magazalar) :
                new ObjectParameter("Magazalar", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_KasaRaporu_Result>("sp_KasaRaporu", basTarihParameter, sonTarihParameter, magazalarParameter);
        }
    
        public virtual ObjectResult<sp_KasaRaporuDeneme_Result> sp_KasaRaporuDeneme()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_KasaRaporuDeneme_Result>("sp_KasaRaporuDeneme");
        }
    
        public virtual ObjectResult<sp_kredi_Result> sp_kredi(string kredi)
        {
            var krediParameter = kredi != null ?
                new ObjectParameter("kredi", kredi) :
                new ObjectParameter("kredi", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_kredi_Result>("sp_kredi", krediParameter);
        }
    
        public virtual int sp_KrediOnayNotu(string alisNo, string notText)
        {
            var alisNoParameter = alisNo != null ?
                new ObjectParameter("AlisNo", alisNo) :
                new ObjectParameter("AlisNo", typeof(string));
    
            var notTextParameter = notText != null ?
                new ObjectParameter("NotText", notText) :
                new ObjectParameter("NotText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_KrediOnayNotu", alisNoParameter, notTextParameter);
        }
    
        public virtual int sp_MasrafYerleri(string basTarih, string sonTarih)
        {
            var basTarihParameter = basTarih != null ?
                new ObjectParameter("BasTarih", basTarih) :
                new ObjectParameter("BasTarih", typeof(string));
    
            var sonTarihParameter = sonTarih != null ?
                new ObjectParameter("SonTarih", sonTarih) :
                new ObjectParameter("SonTarih", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_MasrafYerleri", basTarihParameter, sonTarihParameter);
        }
    
        public virtual int sp_OnayIptal(string birim, Nullable<int> durum, string alisNo, string durumText)
        {
            var birimParameter = birim != null ?
                new ObjectParameter("Birim", birim) :
                new ObjectParameter("Birim", typeof(string));
    
            var durumParameter = durum.HasValue ?
                new ObjectParameter("Durum", durum) :
                new ObjectParameter("Durum", typeof(int));
    
            var alisNoParameter = alisNo != null ?
                new ObjectParameter("AlisNo", alisNo) :
                new ObjectParameter("AlisNo", typeof(string));
    
            var durumTextParameter = durumText != null ?
                new ObjectParameter("DurumText", durumText) :
                new ObjectParameter("DurumText", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_OnayIptal", birimParameter, durumParameter, alisNoParameter, durumTextParameter);
        }
    
        public virtual int YEVMIYEDEFTERISTP(string pSirketNo, Nullable<System.DateTime> pIlkTarih, Nullable<System.DateTime> pSonTarih, ObjectParameter outBorcToplam, ObjectParameter outAlacakToplam, ObjectParameter outSonYevNo)
        {
            var pSirketNoParameter = pSirketNo != null ?
                new ObjectParameter("pSirketNo", pSirketNo) :
                new ObjectParameter("pSirketNo", typeof(string));
    
            var pIlkTarihParameter = pIlkTarih.HasValue ?
                new ObjectParameter("pIlkTarih", pIlkTarih) :
                new ObjectParameter("pIlkTarih", typeof(System.DateTime));
    
            var pSonTarihParameter = pSonTarih.HasValue ?
                new ObjectParameter("pSonTarih", pSonTarih) :
                new ObjectParameter("pSonTarih", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("YEVMIYEDEFTERISTP", pSirketNoParameter, pIlkTarihParameter, pSonTarihParameter, outBorcToplam, outAlacakToplam, outSonYevNo);
        }
    
        public virtual ObjectResult<GetTahsVadeFarkli_Result> GetTahsVadeFarkli(string beginDate, string endDate)
        {
            var beginDateParameter = beginDate != null ?
                new ObjectParameter("beginDate", beginDate) :
                new ObjectParameter("beginDate", typeof(string));
    
            var endDateParameter = endDate != null ?
                new ObjectParameter("endDate", endDate) :
                new ObjectParameter("endDate", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTahsVadeFarkli_Result>("GetTahsVadeFarkli", beginDateParameter, endDateParameter);
        }
    
        public virtual ObjectResult<GetGunSayilariOnay_Result> GetGunSayilariOnay()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetGunSayilariOnay_Result>("GetGunSayilariOnay");
        }
    
        public virtual ObjectResult<GetIleriTeslim_Result> GetIleriTeslim()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetIleriTeslim_Result>("GetIleriTeslim");
        }
    
        public virtual ObjectResult<GetSatisElemanlari_Result> GetSatisElemanlari(string basTarih, string sonTarih)
        {
            var basTarihParameter = basTarih != null ?
                new ObjectParameter("BasTarih", basTarih) :
                new ObjectParameter("BasTarih", typeof(string));
    
            var sonTarihParameter = sonTarih != null ?
                new ObjectParameter("SonTarih", sonTarih) :
                new ObjectParameter("SonTarih", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetSatisElemanlari_Result>("GetSatisElemanlari", basTarihParameter, sonTarihParameter);
        }
    }
}
