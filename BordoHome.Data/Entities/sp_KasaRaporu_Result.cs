//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BordoHome.Data.Entities
{
    using System;
    
    public partial class sp_KasaRaporu_Result
    {
        public string ODEMETIPI { get; set; }
        public string ODMAADI { get; set; }
        public string TAHSILATTIPI { get; set; }
        public string SATISTIPI { get; set; }
        public Nullable<decimal> TUTAR { get; set; }
    }
}
