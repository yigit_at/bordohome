//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BordoHome.Data.Entities
{
    using System;
    
    public partial class GetTahsVadeFarkli_Result
    {
        public string TAHSUBE { get; set; }
        public string SUBEADI { get; set; }
        public Nullable<decimal> TUTAR { get; set; }
        public Nullable<decimal> VADEFARKI { get; set; }
        public Nullable<decimal> ERKENISKONTOSU { get; set; }
    }
}
