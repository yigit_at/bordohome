﻿using System;

namespace BordoHome.ViewModel
{
    public class ProfitMaster
    {
        public DateTime ZMN { get; set; }

        public Int16 FATKOD { get; set; }

        public string SUBEADI { get; set; }

        public string STSEADI { get; set; }

        public string FYKKOD { get; set; }

        public string MSTSATISTIP { get; set; }

        public string MSTTESLIMTIP { get; set; }

        public string MUSTERIADI { get; set; }

        public string MUSTERIKOD { get; set; }

        public string GRPADI { get; set; }

        public string URUNCINSI { get; set; }

        public string MARKA { get; set; }
        
        public bool KDVDHLHSP { get; set; }

        public decimal SATISFIYATI { get; set; }

        public decimal MIKTAR { get; set; }

        public decimal TUTAR { get; set; }

        public decimal ISKONTO { get; set; }

        public decimal KDV { get; set; }

        public decimal NET { get; set; }

        public decimal BRIMMALIYET { get; set; }

        public decimal KDVORANI { get; set; }

        public decimal PESINFIYAT { get; set; }

        public Int16 TAKSAY { get; set; }

        public string IADE { get; set; }
        

    }
}