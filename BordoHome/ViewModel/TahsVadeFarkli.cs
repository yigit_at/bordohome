﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BordoHome.ViewModel
{
    public class TahsVadeFarkli
    {
        public string TAHSUBE { get; set; }

        public string SUBEADI { get; set; }

        public decimal TUTAR { get; set; }

        public decimal VADEFARKI { get; set; }

        public decimal ERKENISKONTOSU { get; set; }
    }
}