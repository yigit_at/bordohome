﻿namespace BordoHome.ViewModel
{
    public class WebCiro
    {
        public string Branch { get; set; }

        public decimal Total { get; set; }

        public decimal Discount { get; set; }

        public decimal Gross { get; set; }
    }
}