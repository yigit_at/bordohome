﻿namespace BordoHome.ViewModel
{
    public class BranchInventory
    {
        public string MARKAADI { get; set; }

        public decimal? MERKEZ { get; set; }

        public decimal? Odemis { get; set; }

        public decimal? Nazilli { get; set; }

        public decimal? Gaziemir { get; set; }

        public decimal? Tire { get; set; }

        public decimal? Milas { get; set; }
    }
}