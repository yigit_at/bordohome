﻿namespace BordoHome.ViewModel
{
    public class DeliverFurtherViewModel
    {
        public string BranchName { get; set; }

        public decimal? BetweenZeroAndTen { get; set; }

        public decimal? BetweenElevenAndThirtyOne { get; set; }

        public decimal? OverThirtyOne { get; set; }

        public decimal? Total { get; set; }
    }
}