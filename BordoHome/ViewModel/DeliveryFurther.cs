﻿namespace BordoHome.ViewModel
{
    public class DeliveryFurther
    {
        public string BranchCode{ get; set; }

        public string BranchName { get; set; }

        public decimal? BetweenZeroAndTen { get; set; }

        public decimal? BetweenElevenAndThirtyOne { get; set; }

        public decimal? OverThirtyOne { get; set; }
    }
}