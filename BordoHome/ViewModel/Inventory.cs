﻿namespace BordoHome.ViewModel
{
    public class Inventory
    {
        public string GRPADI { get; set; }

        public decimal? Merkez { get; set; }

        public decimal? Odemis { get; set; }

        public decimal? Nazilli { get; set; }

        public decimal? Gaziemir { get; set; }

        public decimal? Tire { get; set; }

        public decimal? Milas { get; set; }
    }
}