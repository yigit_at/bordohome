﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BordoHome.ViewModel
{
    public class PurchaseParts
    {
        public string STSEKOD { get; set; }

        public string STSEADI { get; set; }

        public string GRPADI { get; set; }

        public int YIL { get; set; }

        public int AY { get; set; }

        public decimal SATISMIKTAR { get; set; }

        public decimal IADEMIKTAR { get; set; }

        public decimal SATISTUTAR { get; set; }

        public decimal IADETUTAR { get; set; }

        public decimal NET { get; set; }

        public long STSECARI { get; set; }

        public string STSESUBE { get; set; }

        public string Bolum { get; set; }

        public decimal Birinci { get; set; }

        public decimal Ikinci { get; set; }

        public decimal Super { get; set; }

    }
}