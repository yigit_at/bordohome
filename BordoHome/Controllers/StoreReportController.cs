﻿using BordoHome.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BordoHome.Controllers
{
    public class StoreReportController : BaseController
    {

        // GET: StoreReport
        public ActionResult WebCiro()
        {
            string beginDate = "01.10.2017";
            string endDate = "30.10.2017";
            var list = dbContext.Database.SqlQuery<WebCiro>("exec GetWebCiro @beginDate, @endDate",
                                          new SqlParameter("beginDate", beginDate), new SqlParameter("endDate", endDate)).ToList();


            if (list is null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View();
        }


        public ActionResult PaymentDelayCharge()
        {
            string beginDate = "1.09.2017";
            string endDate = "30.09.2017";
            var list = dbContext.Database.SqlQuery<TahsVadeFarkli>("exec GetTahsVadeFarkli @beginDate, @endDate",
                                          new SqlParameter("beginDate", beginDate), new SqlParameter("endDate", endDate)).ToList();

            if (list is null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(list);
        }

        public ActionResult Inventory()
        {
            var list = dbContext.Database.SqlQuery<Inventory>("exec sp_depoenvanter").ToList();

            if (list is null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(list);
        }

        public ActionResult BrandInventory()
        {
            var list = dbContext.Database.SqlQuery<BranchInventory>("exec sp_depoenvanter_marka").ToList();

            ViewData["Merkez"] = list.Sum(o => o.MERKEZ);

            if (list is null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View(list);
        }

        public ActionResult Delivery()
        {
            List<DeliveryFurther> deliveryList = dbContext.Database.SqlQuery<DeliveryFurther>("exec GetIleriTeslim").ToList();

            if (deliveryList is null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View();
        }
        
        public async Task<ActionResult> PurchaseParts()
        {
            var beginDate = $"07.05.2017";
            var endDate = $"07.09.2017";

            var list = await dbContext.Database.SqlQuery<PurchaseParts>("exec GetSatisElemanlari @BasTarih, @SonTarih",
                                          new SqlParameter("BasTarih", beginDate), new SqlParameter("SonTarih", endDate)).ToListAsync();

            if (list is null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);


            return View(list);



        }

    }
}