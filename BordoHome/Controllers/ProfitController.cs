﻿using BordoHome.ViewModel;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BordoHome.Controllers
{
    public class ProfitController : BaseController
    {
        // GET: Profit
        public async Task<ActionResult> Product()
        {
            string branch = "03";
            string beginDate = "2017.01.10";
            string endDate = "2017.10.30";

            var list = await dbContext.Database.SqlQuery<ProfitMaster>("exec sp_KarlilikMaster @branch, @BasTarih, @SonTarih",
                                          new SqlParameter("branch", branch), 
                                          new SqlParameter("BasTarih", beginDate),
                                          new SqlParameter("SonTarih", endDate)).ToListAsync();


            if (list is null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            return View();
        }
    }
}