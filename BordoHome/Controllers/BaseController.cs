﻿using BordoHome.Data.Entities;
using System.Web.Mvc;

namespace BordoHome.Controllers
{
    public abstract class BaseController : Controller
    {
        #region Member

        protected ASBDB_GALERIBORDO01Entities dbContext { get; set; }
        
        #endregion

        public BaseController()
        {
            dbContext = new ASBDB_GALERIBORDO01Entities(); 
        }

        protected override void Dispose(bool disposing)
        {
            if (dbContext != null)
                dbContext.Dispose();
            base.Dispose(disposing);
        }
    }
}